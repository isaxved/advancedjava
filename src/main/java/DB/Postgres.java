package DB;

import java.sql.*;

public class Postgres {

    private static Connection connection = null;

    public static Connection connection() throws SQLException {
        final String psqlUser = "postgres";
        final String psqlPassword = "12345";
        final String psqlUrl = "jdbc:postgresql://localhost/jdbctest";
        connection = DriverManager.getConnection(psqlUrl,psqlUser,psqlPassword);
        return connection;
    }

    public void closeConnection() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

}
